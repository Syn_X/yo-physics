﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class CapacitorRandomSparks : MonoBehaviour
{
    public GameObject spark;
    public float randomSparkAppearanceTime = 0f;
    public int sparkAppearanceRate = 0;

    private Vector3 capacitorDimension;
    private float countdown;
    private float xPadding = 5f;
    private float yPadding = 2f;

    private void Start()
    {
        countdown = randomSparkAppearanceTime;
        capacitorDimension = gameObject.GetComponent<SpriteRenderer>().bounds.size;
    }

    private void Update()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            countdown -= Time.deltaTime;
            if (countdown < 0)
            {
                countdown += randomSparkAppearanceTime;
                if (Random.Range(0, 101) <= sparkAppearanceRate)
                {
                    float posX = gameObject.transform.position.x + Random.value * RandomSign() * (capacitorDimension.x / 2 - xPadding);
                    float posY = gameObject.transform.position.y + Random.value * RandomSign() * (capacitorDimension.y / 2 - yPadding);
                    GameObject obj = GameObject.Instantiate(spark, gameObject.transform);
                    obj.transform.position = new Vector3(posX, posY, 0);
                }
            }
        }
    }

    private int RandomSign()
    {
        return Random.value < 0.5 ? 1 : -1;
    }

}
