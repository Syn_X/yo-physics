﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectroLabLevelManager : MonoBehaviour
{
    private static float SPAWN_Y_PADDING = 17.5f;
    private static float DIFFICULTY_TIMER_DECREMENT = 0.01f;
    private static float SCREEN_X_BOUND = 125f;

    [SerializeField]
    private ElectroLabSceneManager gameHandler;

    public GameObject[] enemies;
    public float startingSpawnTime = 0f;
    public float minSpawnTime = 0.4f;

    private float spawnCountdown;

    private float incrementDifficultyTime = 1f;
    private float incrementDifficultyCountdown = 1f;

    private Camera mainCamera;

    private void Start()
    {
        mainCamera = Camera.main;
        spawnCountdown = startingSpawnTime;
    }

    private void Update()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            if (startingSpawnTime > minSpawnTime)
            {
                incrementDifficultyCountdown -= Time.deltaTime;
                if (incrementDifficultyCountdown < 0)
                {
                    incrementDifficultyCountdown += incrementDifficultyTime;
                    startingSpawnTime -= DIFFICULTY_TIMER_DECREMENT;
                }
            }

            spawnCountdown -= Time.deltaTime;

            if (spawnCountdown < 0)
            {
                GameObject obj = Instantiate(enemies[Random.Range(0, enemies.Length)]);

                Quaternion rotation = obj.CompareTag("RotatingEnemy") ? Random.rotation : Quaternion.identity;
                rotation = new Quaternion(0, 0, rotation.x, rotation.w);

                obj.transform.position = new Vector3(SCREEN_X_BOUND, Random.Range(-mainCamera.orthographicSize + SPAWN_Y_PADDING, mainCamera.orthographicSize - SPAWN_Y_PADDING), 0);
                obj.transform.rotation = rotation;

                spawnCountdown += startingSpawnTime;

                obj.GetComponent<EnemyManager>().increaseScoreEvent.AddListener(() => {
                    gameHandler.IncrementScore();
                }
                );
            }
        }

    }
}
