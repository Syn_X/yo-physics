﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ElectroLabSceneManager : MonoBehaviour
{
    public SceneTransitionManager transition;

    public TextMeshProUGUI inGameScore;

    public GameObject inGameMenu;
    public GameObject infoPanel;

    public GameObject endGameMenu;
    public TextMeshProUGUI menuScoreText;
    public TextMeshProUGUI menuHighscoreText;

    private int score = 0;
    private bool isFirstOpening = false;

    private void Awake()
    {
        Input.backButtonLeavesApp = false;
        isFirstOpening = PlayerPrefs.GetInt("firstOpening_ElectroLab", 0) == 0;
    }

    private void Start()
    {
        GameState.Instance.State = GameStates.Paused;
        if (isFirstOpening)
        {
            ShowInfo();
            PlayerPrefs.SetInt("firstOpening_ElectroLab", 1);
        }
    }

    public void IncrementScore()
    {
        if (GameState.Instance.State != GameStates.Running)
            return;

        score++;
        inGameScore.text = score.ToString();
    }

    public void GamePaused()
    {
        GameState.Instance.State = GameStates.Paused;
        ShowInGameMenu();
    }

    public void GameResumed()
    {
        HideInGameMenu();
    }

    public void GameEnded()
    {
        GameState.Instance.State = GameStates.Ended;
        menuScoreText.text = score.ToString();

        if (score > PlayerPrefs.GetInt("highscore_ElectroLab", 0))
        {
            PlayerPrefs.SetInt("highscore_ElectroLab", score);
            menuHighscoreText.text = score.ToString();
        } else
        {
            menuHighscoreText.text = PlayerPrefs.GetInt("highscore_ElectroLab", 0).ToString();
        }

        ShowEndGameMenu();
    }

    public void PlayAgain()
    {
        transition.LoadScene("ElectroLabScene");
    }

    public void Exit()
    {
        transition.LoadScene("MainMenuScene");
    }

    public void ShowInfo()
    {
        GameState.Instance.State = GameStates.Info;
        infoPanel.SetActive(true);
        CanvasGroup canvas = infoPanel.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    public void CloseInfo()
    {
        GameState.Instance.State = GameStates.Paused;
        CanvasGroup canvas = infoPanel.GetComponent<CanvasGroup>();
        StartCoroutine(FadeOut(canvas, 0.5f));
    }

    private void ShowEndGameMenu()
    {
        endGameMenu.SetActive(true);
        CanvasGroup canvas = endGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    private void ShowInGameMenu()
    {
        inGameMenu.SetActive(true);
        CanvasGroup canvas = inGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    private void HideInGameMenu()
    {
        CanvasGroup canvas = inGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeOutAndResumeGame(canvas, 0.5f));
    }

    private void ResumeGame()
    {
        GameState.Instance.State = GameStates.Running;
    }

    private IEnumerator FadeIn(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime/fadingTime;
            yield return null;
        }
    }

    private IEnumerator FadeOutAndResumeGame(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / fadingTime;
            yield return null;
        }
        canvasGroup.gameObject.SetActive(false);
        ResumeGame();
    }

    private IEnumerator FadeOut(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / fadingTime;
            yield return null;
        }
        canvasGroup.gameObject.SetActive(false);
    }

    

}
