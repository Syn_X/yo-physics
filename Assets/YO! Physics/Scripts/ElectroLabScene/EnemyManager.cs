﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EnemyManager : MonoBehaviour
{
    private static float SCREEN_X_BOUND = 125f;

    [HideInInspector]
    public UnityEvent increaseScoreEvent;

    public float velocity = 0f;

    private Transform player;

    private void OnEnable()
    {
        player = FindObjectOfType<InputForce>().transform;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameState.Instance.State == GameStates.Running)
            Move();
    }

    private void Move()
    {
        bool wasOnRight = IsOnPlayersRight();

        transform.position += new Vector3(-1, 0, 0) *  velocity * Time.deltaTime;

        bool isOnRight = IsOnPlayersRight();

        if(wasOnRight && !isOnRight)
        {
            increaseScoreEvent.Invoke();
        }

        if (gameObject.transform.position.x < -SCREEN_X_BOUND)
        {
            Destroy(this.gameObject);
        }
    }

    private bool IsOnPlayersRight()
    {
        return transform.position.x > player.transform.position.x;
    }
}
