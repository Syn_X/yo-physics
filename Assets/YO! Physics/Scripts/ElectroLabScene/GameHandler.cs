﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameHandler : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    private int score = 0;

    public void IncrementScore()
    {
        score++;
        scoreText.text = score.ToString();
    }
}
