﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class InputForce : MonoBehaviour
{
    public UnityEvent voltageChangedEvent;

    private bool mouseDown = false;
    public Rigidbody2D rb;
    public float EM_Constant;

    private void Update()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            if (Input.GetMouseButtonUp(0))
            {
                mouseDown = false;
                voltageChangedEvent.Invoke();
            }
            if (Input.GetMouseButtonDown(0))
            {
                mouseDown = true;
                voltageChangedEvent.Invoke();
            }
        } else
        {
            if (mouseDown) 
                mouseDown = false;
        }
    }

    private void FixedUpdate()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            ApplyEMForce();
        } else
        {
            ResetEMForce();
        }
    }

    private void ApplyEMForce()
    {
        // Apply velocity and not force because the game must be playable.
        // Unfortunately this is not the proper physics behind EM field inside a capacitor
        if (mouseDown)
        {
            rb.velocity = Vector2.up * EM_Constant;
        }
        else
        {
            rb.velocity = Vector2.down * EM_Constant;
        }

        //Physically accurate force
        //if (mouseDown)
        //{
        //    rb.AddForce(Vector2.up * EM_Constant, ForceMode2D.Force);
        //}
        //else
        //{
        //    rb.AddForce(Vector2.down * EM_Constant, ForceMode2D.Force);
        //}
    }

    private void ResetEMForce()
    {
        rb.velocity = Vector2.zero;
    }

}
