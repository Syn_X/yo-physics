﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGameAssets : MonoBehaviour
{
    private static MyGameAssets _instance;

    public static MyGameAssets Instance { get { return _instance; } }

    //public Sprite electronSprite;
    //public Sprite protonSprite;
    //public Sprite neutronSprite;
    //public Sprite quarkUpSprite;
    //public Sprite quarkDownSprite;
    //public Sprite neutrinoSprite;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
}
