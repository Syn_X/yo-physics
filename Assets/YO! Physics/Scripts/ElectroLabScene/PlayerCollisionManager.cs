﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class PlayerCollisionManager : MonoBehaviour
{
    public GameObject collisionVFX;

    public UnityEvent playerCollidedEvent;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector2 collisionPoint = collision.contacts[0].point;
        GameObject obj = GameObject.Instantiate(collisionVFX, new Vector3(collisionPoint.x, collisionPoint.y, 0), Quaternion.identity);
        playerCollidedEvent.Invoke();
    }

}
