﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialPanelBehaviour : MonoBehaviour
{

    public CanvasGroup canvasGroup;

    private void Update()
    {
        if (GameState.Instance.State == GameStates.Paused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartGame();
            }
        }
    }

    private void StartGame()
    {
        StartCoroutine(FadeOut(1f));
    }

    private IEnumerator FadeOut(float fadingTime)
    {
        while(canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime;
            yield return null;
        }
        gameObject.SetActive(false);
        GameState.Instance.State = GameStates.Running;
    }
}
