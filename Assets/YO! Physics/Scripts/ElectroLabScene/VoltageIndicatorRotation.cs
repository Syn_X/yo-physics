﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoltageIndicatorRotation : MonoBehaviour
{
    
    public GameObject voltageVFX;

    public void OnVoltageChange()
    {
        Vector3 targetAngle = transform.eulerAngles + 180f * Vector3.forward;

        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, targetAngle, 1f);

        GameObject obj = GameObject.Instantiate(voltageVFX, gameObject.transform);
    }

}
