﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{

    public Sprite[] bgSprites;

    public GameObject bg_1;
    public GameObject bg_2;

    private SpriteRenderer bg_1_spriteRenderer;
    private SpriteRenderer bg_2_spriteRenderer;

    public float timeBeforeFading = 0f;
    public float fadingTime = 0f;

    private float shouldChangeTimer = 0f;
    private float shouldZoomTimer = 0f;

    private int currentInactiveSpriteIndex = 1;

    private bool bg_1_active = true;

    private bool isFading = false;

    // Start is called before the first frame update
    void Start()
    {
        bg_1_spriteRenderer = bg_1.GetComponent<SpriteRenderer>();
        bg_2_spriteRenderer = bg_2.GetComponent<SpriteRenderer>();

        ResetAll();

        shouldChangeTimer = timeBeforeFading;
        shouldChangeTimer = timeBeforeFading + fadingTime;
    }

    // Update is called once per frame
    void Update()
    {
        if(GameState.Instance.State == GameStates.Running)
        {
            if (shouldChangeTimer < 0)
            {
                // Change background!
                StartCoroutine(ChangeBackground());
                shouldChangeTimer += timeBeforeFading;
            }
            else if (!isFading)
            {
                shouldChangeTimer -= Time.deltaTime;
            }

            if (shouldZoomTimer < 0)
            {
                shouldChangeTimer += timeBeforeFading + fadingTime;
            }
            else
            {
                GetActiveBackground().transform.localScale = GetActiveBackground().transform.localScale * 1.00005f;
            }
        }
    }

    GameObject GetActiveBackground()
    {
        return (bg_1_active) ? bg_1 : bg_2;   
    }

    SpriteRenderer GetActiveSpriteRenderer()
    {
        return (bg_1_active) ? bg_1_spriteRenderer : bg_2_spriteRenderer;
    }

    GameObject GetInactiveBackground()
    {
        return (!bg_1_active) ? bg_1 : bg_2;
    }

    SpriteRenderer GetInactiveSpriteRenderer()
    {
        return (!bg_1_active) ? bg_1_spriteRenderer : bg_2_spriteRenderer;
    }

    IEnumerator ChangeBackground()
    {
        isFading = true;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / fadingTime)
        {
            Color newColor = new Color(1, 1, 1, Mathf.Lerp(1f, 0f, t));
            GetActiveSpriteRenderer().material.color = newColor;
            newColor = new Color(1, 1, 1, Mathf.Lerp(0f, 1f, t));
            GetInactiveSpriteRenderer().material.color = newColor;
            yield return null;
        }

        bg_1_active = !bg_1_active;

        ResetAll();
        ChangeInactiveBackground();
        isFading = false;
    }

    void ResetAll()
    {
        Color color = new Color(1, 1, 1, 1);
        GetActiveSpriteRenderer().material.color = color;
        color.a = 0.0f;
        GetInactiveSpriteRenderer().material.color = color;
        bg_1.transform.localScale = Vector3.one;
        bg_2.transform.localScale = Vector3.one;

    }

    void ChangeInactiveBackground()
    {
        currentInactiveSpriteIndex++;
        GetInactiveSpriteRenderer().sprite = bgSprites[currentInactiveSpriteIndex % bgSprites.Length];
    }

}