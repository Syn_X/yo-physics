﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class BlackHoleBehaviour : MonoBehaviour
{

    public Rigidbody2D rb;
    public PostProcessVolume pp;

    Bloom bloom = null;
    ChromaticAberration aberration = null;
    Vignette vignette = null;

    private float massIncreaseSpeed = 750;
    private float scaleIncreaseSpeed= 0.75f;
    private float bloomIncreaseSpeed= 1.5f;
    private float aberrationIncreaseSpeed= 0.75f;
    private float vignetteIncreaseSpeed= 0.005f;

    private void Awake()
    {
        pp = FindObjectOfType<PostProcessVolume>();
        pp.profile.TryGetSettings(out bloom);
        pp.profile.TryGetSettings(out aberration);
        pp.profile.TryGetSettings(out vignette);
    }

    // Update is called once per frame
    void Update()
    {
        rb.mass += massIncreaseSpeed * Time.deltaTime;
        transform.localScale = new Vector3(transform.localScale.x + scaleIncreaseSpeed * Time.deltaTime, transform.localScale.y + scaleIncreaseSpeed * Time.deltaTime, 1);
        bloom.intensity.value += bloomIncreaseSpeed * Time.deltaTime;
        aberration.intensity.value += aberrationIncreaseSpeed * Time.deltaTime;
        vignette.intensity.value += vignetteIncreaseSpeed * Time.deltaTime;
    }
}
