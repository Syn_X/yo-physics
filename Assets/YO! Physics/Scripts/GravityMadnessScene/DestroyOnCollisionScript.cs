﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnCollisionScript : MonoBehaviour
{
    public GameObject explosion;
    public PlayAudioSourceScript audioSource;
    private bool isInScreen = false;


    public void EnteredScreen()
    {
        isInScreen = true;
    }

    public void ExitedScreen()
    {
        isInScreen = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (!isInScreen) 
            return;

        if (!collision.gameObject.CompareTag("BlackHole"))
        {
            if (audioSource != null)
            {
                audioSource.PlayOneShot();
            }

            if (explosion != null)
            {
                Instantiate(explosion, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
            }
            Destroy(this.gameObject);
        }
        else
        {
            StartCoroutine(ObjectFadeOut());
        }
    }

    private IEnumerator ObjectFadeOut()
    {
        float speed = 0.05f;
        while (transform.localScale.x < 0.01)
        {
            transform.localScale = new Vector3(transform.localScale.x - speed, transform.localScale.y - speed, 1);
            yield return null;
        }
        Destroy(this.gameObject);
    }

}
