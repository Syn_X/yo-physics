﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMovement : MonoBehaviour
{
    public Rigidbody2D rb;
    public Vector2 swipeDirection { get; private set; }

    private Vector2 initialTouchPosition;
    private Vector2 finalTouchPosition;

    private Camera mainCam;

    private float dragStartTime = 0f;
    private float dragTime = 0f;

    private float dragDistance = 0f;

    private bool applyForce = false;

    public int forceMultiplier = 10;


    #region Unity functions
    void Start()
    {
        mainCam = Camera.main;
    }

    void FixedUpdate()
    {
        if(GameState.Instance.State == GameStates.Running)
        {
            if (applyForce)
            {
                float forceIntensity = (dragDistance / dragTime) * forceMultiplier;
                rb.AddForce((finalTouchPosition - new Vector2(transform.position.x, transform.position.y)).normalized * forceIntensity, ForceMode2D.Force);
                ResetInitialState();
            }
        }
    }

    #endregion

    #region Interface functions

    // Using this works on mobile but it doesn't allow multi-touch
    private void OnMouseDown()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            initialTouchPosition = mainCam.ScreenToWorldPoint(Input.mousePosition);

            finalTouchPosition = Vector2.zero;
            dragStartTime = Time.realtimeSinceStartup;
        }
    }

    private void OnMouseUp()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            finalTouchPosition = mainCam.ScreenToWorldPoint(Input.mousePosition);
            dragTime = Time.realtimeSinceStartup - dragStartTime;

            dragDistance = Mathf.Max(Mathf.Abs(finalTouchPosition.y - initialTouchPosition.y), Mathf.Abs(finalTouchPosition.x - initialTouchPosition.x));

            applyForce = true;
        }
    }

    #endregion

    #region Utilities

    private void ResetInitialState()
    {
        dragStartTime = 0f;
        dragTime = 0f;
        dragDistance = 0f;

        applyForce = false;

        initialTouchPosition = Vector2.zero;
        finalTouchPosition = Vector2.zero;
    }

    #endregion

}
