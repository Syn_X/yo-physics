﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gravity : MonoBehaviour
{
    public GameObject movableObjects;
    public int gravityConstant = 0;


    void FixedUpdate()
    {
        if(GameState.Instance.State == GameStates.Running)
        {
            foreach (Rigidbody2D rb in movableObjects.GetComponentsInChildren<Rigidbody2D>())
            {

                foreach (Rigidbody2D rb2 in movableObjects.GetComponentsInChildren<Rigidbody2D>())
                {
                    if (rb2 == rb) continue;

                    Vector3 dst = rb2.position - rb.position;

                    Vector3 force = gravityConstant * dst.normalized * (rb2.mass * rb.mass) / (dst.sqrMagnitude);
                    if (force.sqrMagnitude > 0.1)
                    {
                        rb.AddForce(force, ForceMode2D.Force);
                    }
                }

            }
        }
 
    }

}
