﻿using System.Collections;
using UnityEngine;

public enum PhaseNames
{
    None,
    PhaseAsteroid,
    PhaseComet,
    PhaseAsteroidComet,
    PhaseGiant,
    PhaseSuperGiant,
    PhaseBlackHole
}

public class Phase
{
    public PhaseNames name;
    public float duration;
    public GameObject obj = null;

    public Phase(PhaseNames n, float d, GameObject r = null)
    {
        name = n;
        duration = d;
        obj = null;
    }
}

public class GravityMadnessLevelManager : MonoBehaviour
{
    public SpawnScript asteroidSpawner;
    public SpawnScript cometSpawner;
    public SpawnOneShotScript centralSpawner;

    public GameObject giantStar;
    public GameObject superGiantStar;
    public GameObject blackHole;

    public PlayAudioSourceScript ambienceAudio;
    private bool ambienceAudioOn = false;

    private float elapsedTime;

    private float difficultyIncrementTime = 5f;

    private int currentPhaseIndex = 0;
    private Phase currentPhase;
    private float currentDuration;

    public Phase[] orderedGamePhases;

    private void Awake()
    {
        orderedGamePhases = new Phase[]{
            new Phase(PhaseNames.PhaseAsteroid, 5f),
            new Phase(PhaseNames.PhaseComet, 5f),
            new Phase(PhaseNames.PhaseAsteroidComet, 20f),
            new Phase(PhaseNames.PhaseGiant, 25f, giantStar),
            new Phase(PhaseNames.PhaseAsteroidComet, 20f),
            new Phase(PhaseNames.PhaseSuperGiant, 30f, superGiantStar),
            new Phase(PhaseNames.PhaseAsteroidComet, 20f),
            new Phase(PhaseNames.PhaseBlackHole, 15f, blackHole)
        };
    }

    private void Start()
    {
        currentPhase = orderedGamePhases[currentPhaseIndex];
    }

    private void Update()
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            switch (currentPhase.name)
            {
                case PhaseNames.PhaseAsteroid:
                    DoPhaseAsteroid();
                    break;
                case PhaseNames.PhaseComet:
                    DoPhaseComet();
                    break;
                case PhaseNames.PhaseAsteroidComet:
                    DoPhaseAsteroidComet();
                    break;
                case PhaseNames.PhaseGiant:
                    DoPhaseGiant();
                    break;
                case PhaseNames.PhaseSuperGiant:
                    DoPhaseSuperGiant();
                    break;
                case PhaseNames.PhaseBlackHole:
                    DoPhaseBlackHole();
                    break;
                default:
                    return;
            }
        }
    }

    private void EndPhase()
    {
        ResetEnabledObjects();
        currentPhase = new Phase(PhaseNames.None, 0);
        StartCoroutine(PhaseTransition());
    }

    private void ResetEnabledObjects()
    {
        asteroidSpawner.shouldSpawn = false;
        cometSpawner.shouldSpawn = false;
        centralSpawner.shouldSpawn = false;
    }

    private IEnumerator PhaseTransition()
    {
        yield return new WaitForSeconds(1);

        currentPhaseIndex++;
        currentPhase = orderedGamePhases[currentPhaseIndex];
    }

    private void DoPhaseAsteroid()
    {
        if (elapsedTime > currentPhase.duration)
        {
            elapsedTime = 0f;
            EndPhase();
        } else
        {
            elapsedTime += Time.deltaTime;
            asteroidSpawner.shouldSpawn = true;
        }
    }

    private void DoPhaseComet()
    {
        if (elapsedTime > currentPhase.duration)
        {
            elapsedTime = 0f;
            EndPhase();
        }
        else
        {
            elapsedTime += Time.deltaTime;
            cometSpawner.shouldSpawn = true;
        }
    }

    private void DoPhaseAsteroidComet()
    {
        if (elapsedTime > currentPhase.duration)
        {
            elapsedTime = 0f;
            EndPhase();
        }
        else
        {
            elapsedTime += Time.deltaTime;
            asteroidSpawner.shouldSpawn = true;
            cometSpawner.shouldSpawn = true;
        }
    }

    private void DoPhaseGiant()
    {
        EnableAmbienceAudio();

        if (elapsedTime > currentPhase.duration)
        {
            DisableAmbienceAudio();
            elapsedTime = 0f;
            centralSpawner.DestroyObject();
            EndPhase();
        }
        else
        {
            elapsedTime += Time.deltaTime;
            centralSpawner.spawningObject = giantStar;
            centralSpawner.shouldSpawn = true;
            if(elapsedTime > difficultyIncrementTime)
            {
                asteroidSpawner.shouldSpawn = true;
                cometSpawner.shouldSpawn = true;
            }
        }
    }

    private void DoPhaseSuperGiant()
    {
        EnableAmbienceAudio();

        if (elapsedTime > currentPhase.duration)
        {
            DisableAmbienceAudio();
            elapsedTime = 0f;
            centralSpawner.DestroyObject();
            EndPhase();
        }
        else
        {
            elapsedTime += Time.deltaTime;
            centralSpawner.spawningObject = superGiantStar;
            centralSpawner.shouldSpawn = true;
            if (elapsedTime > difficultyIncrementTime)
            {
                asteroidSpawner.shouldSpawn = true;
                cometSpawner.shouldSpawn = true;
            }
        }
    }

    private void DoPhaseBlackHole()
    {
        EnableAmbienceAudio();
        elapsedTime += Time.deltaTime;
        centralSpawner.spawningObject = blackHole;
        centralSpawner.shouldSpawn = true;
        asteroidSpawner.shouldSpawn = true;
        cometSpawner.shouldSpawn = true;
    }

    private void EnableAmbienceAudio()
    {
        if (!ambienceAudioOn)
        {
            ambienceAudio.PlayOneShot();
            ambienceAudioOn = true;
        }
    }

    private void DisableAmbienceAudio()
    {
        ambienceAudio.Stop();
        ambienceAudioOn = false;
    }
}