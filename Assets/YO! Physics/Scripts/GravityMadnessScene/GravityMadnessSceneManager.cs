﻿using System.Collections;
using TMPro;
using UnityEngine;

public class GravityMadnessSceneManager : MonoBehaviour
{
    public SceneTransitionManager transition;

    public TextMeshProUGUI inGameScore;

    public GameObject inGameMenu;
    public GameObject infoPanel;

    public GameObject endGameMenu;
    public TextMeshProUGUI menuScoreText;
    public TextMeshProUGUI menuLastingLifeText;
    public TextMeshProUGUI menuTotalText;
    public TextMeshProUGUI menuHighscoreText;

    private int score = 0;
    private int lastingLife = 0;
    private int totalScore = 0;
    private float incrementScoreTime = 1f;
    private float elapsedTime = 0f;
    private bool isFirstOpening = false;

    private void Awake()
    {
        Input.backButtonLeavesApp = false;
        isFirstOpening = PlayerPrefs.GetInt("firstOpening_GravityMadness", 0) == 0;
    }

    private void Start()
    {
        GameState.Instance.State = GameStates.Paused;
        if (isFirstOpening)
        {
            ShowInfo();
            PlayerPrefs.SetInt("firstOpening_GravityMadness", 1);
        }
    }

    private void Update()
    {
        if(GameState.Instance.State == GameStates.Running)
        {
            elapsedTime += Time.deltaTime;
            if(elapsedTime > incrementScoreTime)
            {
                IncrementScore();
                elapsedTime -= incrementScoreTime;
            }
        }
    }

    public void IncrementScore(int value = 1)
    {
        if (GameState.Instance.State != GameStates.Running)
            return;

        score += value;
        inGameScore.text = score.ToString();
    }

    public void OnRemainingLifePointsEvent(int value = 0)
    {
        lastingLife = value;
    }

    public void GamePaused()
    {
        GameState.Instance.State = GameStates.Paused;
        ShowInGameMenu();
    }

    public void GameResumed()
    {
        HideInGameMenu();
    }

    public void GameEnded()
    {
        GameState.Instance.State = GameStates.Ended;
        totalScore = score + lastingLife;

        menuScoreText.text = score.ToString();
        menuLastingLifeText.text = lastingLife.ToString();
        menuTotalText.text = totalScore.ToString();

        if (totalScore > PlayerPrefs.GetInt("highscore_GravityMadness", 0))
        {
            PlayerPrefs.SetInt("highscore_GravityMadness", totalScore);
            menuHighscoreText.text = totalScore.ToString();
        }
        else
        {
            menuHighscoreText.text = PlayerPrefs.GetInt("highscore_GravityMadness", 0).ToString();
        }

        ShowEndGameMenu();
    }

    public void PlayAgain()
    {
        transition.LoadScene("GravityMadnessScene");
    }

    public void Exit()
    {
        transition.LoadScene("MainMenuScene");
    }

    public void ShowInfo()
    {
        GameState.Instance.State = GameStates.Info;
        infoPanel.SetActive(true);
        CanvasGroup canvas = infoPanel.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    public void CloseInfo()
    {
        GameState.Instance.State = GameStates.Paused;
        CanvasGroup canvas = infoPanel.GetComponent<CanvasGroup>();
        StartCoroutine(FadeOut(canvas, 0.5f));
    }

    private void ShowEndGameMenu()
    {
        endGameMenu.SetActive(true);
        CanvasGroup canvas = endGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    private void ShowInGameMenu()
    {
        inGameMenu.SetActive(true);
        CanvasGroup canvas = inGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));
    }

    private void HideInGameMenu()
    {
        CanvasGroup canvas = inGameMenu.GetComponent<CanvasGroup>();
        StartCoroutine(FadeOutAndResumeGame(canvas, 0.5f));
    }

    private void ResumeGame()
    {
        GameState.Instance.State = GameStates.Running;
    }

    private IEnumerator FadeIn(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime / fadingTime;
            yield return null;
        }
    }

    private IEnumerator FadeOutAndResumeGame(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / fadingTime;
            yield return null;
        }
        canvasGroup.gameObject.SetActive(false);
        ResumeGame();
    }

    private IEnumerator FadeOut(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / fadingTime;
            yield return null;
        }
        canvasGroup.gameObject.SetActive(false);
    }

}
