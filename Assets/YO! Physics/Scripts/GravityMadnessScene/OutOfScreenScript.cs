﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class OutOfScreenScript : MonoBehaviour
{

    private Vector2 screenBoundaries;
    private float halfWidth;
    private float halfHeight;
    private Rigidbody2D rb;

    private float timeOutOfScreen = 0f;
    private bool hasEnteredScreen = false;

    public UnityEvent enteredScreenEvent = null;

    void Start()
    {
        screenBoundaries = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        halfWidth = GetComponent<SpriteRenderer>().bounds.size.x / 2f;
        halfHeight = GetComponent<SpriteRenderer>().bounds.size.y / 2f;
        rb = GetComponent<Rigidbody2D>();
    }

    void LateUpdate()
    {
        bool isInScreen = (transform.position.x < screenBoundaries.x + halfWidth && transform.position.x > -screenBoundaries.x - halfWidth) &&
            (transform.position.y < screenBoundaries.y + halfHeight && transform.position.y > -screenBoundaries.y - halfHeight);
        
        if (!isInScreen && hasEnteredScreen)
        {
            GameObject.Destroy(this.gameObject);
        }

        if (isInScreen && !hasEnteredScreen)
        {
            hasEnteredScreen = true;
            enteredScreenEvent.Invoke();
        }
         
        // if spawned object doesn't appear into screen in 1 second, destroy it
        if(!isInScreen && !hasEnteredScreen)
        {
            timeOutOfScreen += Time.deltaTime;
            if (timeOutOfScreen > 1.0f)
            {
                GameObject.Destroy(this.gameObject);
            }
        }
    }
}
