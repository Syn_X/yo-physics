﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class RemainingLifePointsEvent : UnityEvent<int> { }

public class PlanetLifeManager : MonoBehaviour
{
    public int life = 100;
    public GameObject explosionParticleSystem;
    public AudioSource explosionSFX;

    public Sprite[] planetSprites;

    public UnityEvent gameOverEvent;

    public UnityEvent onCollisionEvent;

    public RemainingLifePointsEvent remainingLifeEvent;

    private SpriteRenderer parentSprite;

    private void Awake()
    {
        parentSprite = gameObject.GetComponent<SpriteRenderer>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (GameState.Instance.State == GameStates.Running)
        {
            onCollisionEvent.Invoke();

            if (collision.gameObject.CompareTag("Satellite"))
            {
                life -= 5;
            }
            if (collision.gameObject.CompareTag("Asteroid"))
            {
                life -= 1;
            }
            if (collision.gameObject.CompareTag("Comet"))
            {
                life -= 3;
            }
            if (collision.gameObject.CompareTag("GiantStar"))
            {
                life -= 25;
                BounceFromStar(collision.gameObject.transform.position);
            }
            if (collision.gameObject.CompareTag("SuperGiantStar"))
            {
                life -= 50;
                BounceFromStar(collision.gameObject.transform.position);
            }
            if (collision.gameObject.CompareTag("BlackHole"))
            {
                remainingLifeEvent.Invoke(life);
                life = 0;
            }

            if (life <= 0)
            {
                Explode();
            }
            else
            {
                ChooseSprite();
            }
        }

    }

    private void SetSprite(int index)
    {
        parentSprite.sprite = planetSprites[index];
    }

    private void ChooseSprite()
    {
        if(life > 85)
        {
            SetSprite(0);
        } 
        else if (life > 65)
        {
            SetSprite(1);
        }
        else if (life > 45)
        {
            SetSprite(2);
        }
        else if (life > 25)
        {
            SetSprite(3);
        }
        else if (life > 15)
        {
            SetSprite(4);
        }
        else
        {
            SetSprite(5);
        }
    }

    private void Explode()
    {
        gameOverEvent.Invoke();

        if (explosionParticleSystem != null)
        {
            if (explosionSFX != null)
            {
                explosionSFX.Play();
            }
            Instantiate(explosionParticleSystem, new Vector3(transform.position.x, transform.position.y, transform.position.z), transform.rotation);
        }
        Destroy(this.gameObject);
    }


    private void BounceFromStar(Vector3 otherPosition)
    {
        Vector3 dir = (transform.position - otherPosition).normalized;
        float forceIntensity = 2000f;
        gameObject.GetComponent<Rigidbody2D>().AddForce(dir * forceIntensity, ForceMode2D.Impulse);
    }

}
