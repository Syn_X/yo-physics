﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotationVelocity = 0.1f;

    // Update is called once per frame
    void Update()
    {
        transform.rotation *= new Quaternion(0, 0, Time.deltaTime * rotationVelocity, 1);
    }
}
