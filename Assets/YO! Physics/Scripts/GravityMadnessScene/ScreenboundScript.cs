﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenboundScript : MonoBehaviour
{
    public float velocityAttenuation = 1.0f;

    private Vector2 screenBoundaries;
    private float halfWidth;
    private float halfHeight;
    private Rigidbody2D rb;

    void Start()
    {
        screenBoundaries = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, Screen.height, Camera.main.transform.position.z));
        halfWidth = GetComponent<SpriteRenderer>().bounds.size.x / 2f;
        halfHeight = GetComponent<SpriteRenderer>().bounds.size.y / 2f;
        rb = GetComponent<Rigidbody2D>();
    }

    void LateUpdate()
    {
        Vector3 boundPosition = transform.position;
        boundPosition.x = Mathf.Clamp(boundPosition.x, screenBoundaries.x * -1 + halfWidth, screenBoundaries.x - halfWidth);
        boundPosition.y = Mathf.Clamp(boundPosition.y, screenBoundaries.y * -1 + halfHeight, screenBoundaries.y - halfHeight);
        if(transform.position != boundPosition)
        {
            // Bounce: if left-right border, invert x, else 
            if (transform.position.x != boundPosition.x)
            {
                rb.velocity = rb.velocity * new Vector2(-1, 1) * velocityAttenuation;
            } else
            {
                rb.velocity = rb.velocity * new Vector2(1, -1) * velocityAttenuation;

            }

            transform.position = boundPosition;            
        }
    }
}
