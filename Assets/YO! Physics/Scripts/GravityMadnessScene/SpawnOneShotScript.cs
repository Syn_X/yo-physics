﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class SpawnOneShotScript : MonoBehaviour
{
    public GameObject spawningObject = null;
    public Transform spawnedObjectContainer;

    public PlayAudioSourceScript destroySFX;
    public GameObject destroyParticle;

    public bool shouldSpawn = false;

    private bool hasSpawned = false;
    private GameObject instantiatedObject;

    private Vector3 destScale;

    void Update()
    {
        if (GameState.Instance.State == GameStates.Running && shouldSpawn && !hasSpawned)
        {
            instantiatedObject = Instantiate(spawningObject, transform.position, Quaternion.identity);
            instantiatedObject.SetActive(false);
            destScale = instantiatedObject.transform.localScale;
            instantiatedObject.transform.localScale = Vector3.zero;
            instantiatedObject.transform.parent = spawnedObjectContainer;
            StartCoroutine(ObjectFadeIn());
            hasSpawned = true;
        }
    }

    public void DestroyObject()
    {
        shouldSpawn = false;
        hasSpawned = false;
        if (destroySFX != null)
        {
            destroySFX.PlayOneShot();
        }
        Instantiate(destroyParticle, instantiatedObject.transform.position, Quaternion.identity);
        Destroy(instantiatedObject);
    }

    private IEnumerator ObjectFadeIn()
    {
        instantiatedObject.SetActive(true);
        float speed = 1f * Time.deltaTime;
        while (instantiatedObject.transform.localScale.x < destScale.x)
        {
            instantiatedObject.transform.localScale = new Vector3(instantiatedObject.transform.localScale.x + speed, instantiatedObject.transform.localScale.y + speed, 1);
            yield return null;
        }
    }
}
