﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnScript : MonoBehaviour
{
    public bool shouldSpawn = false;
    public float spawningTime = 1.0f;
    public Rigidbody2D spawningObject = null;
    public Transform spawnedObjectContainer;
    public int maxNumberOfSpawnedObjects = 0;
    public PlayAudioSourceScript audioSourceToAttach;

    public float maxDeviationAngle = 30f;
    public float spawnedObjectForce = 10f;

    private Transform[] spawningPoints;
    private float passedTime = 0;
    
    // Start is called before the first frame update
    void Start()
    {
        passedTime = spawningTime;
        spawningPoints = GetComponentsInChildren<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if(GameState.Instance.State == GameStates.Running && shouldSpawn)
        {
            if (passedTime <= 0)
            {
                int randomIndex = Random.Range(1, spawningPoints.Length);
                int i = Random.Range(1, maxNumberOfSpawnedObjects + 1);
                while (i > 0)
                {
                    Quaternion rndRot = Random.rotation;
                    rndRot.x = 0;
                    rndRot.y = 0;
                    var obj = Instantiate(spawningObject, spawningPoints[randomIndex].position, rndRot);

                    obj.transform.parent = spawnedObjectContainer;
                    obj.AddForce(CreateForceVector(randomIndex), ForceMode2D.Force);

                    float scale = Random.Range(0.2f, 1f);
                    obj.transform.localScale = new Vector3(scale, scale, 1);

                    //obj.GetComponent<Rigidbody2D>().mass = 0.25f * scale;

                    DestroyOnCollisionScript destroy = obj.GetComponent<DestroyOnCollisionScript>();
                    if (destroy != null)
                    {
                        destroy.audioSource = audioSourceToAttach;
                    }

                    i--;
                }

                passedTime += spawningTime;
            }
            passedTime -= Time.deltaTime;
        }
    }

    Vector3 CreateForceVector(int index)
    {
        Vector3 force = (spawningPoints[0].position - spawningPoints[index].position) * spawnedObjectForce;
        force = Quaternion.AngleAxis(Random.Range(-maxDeviationAngle, maxDeviationAngle), Vector3.forward) * force;
        return force;
    }
}
