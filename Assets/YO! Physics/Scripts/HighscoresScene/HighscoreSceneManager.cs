﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HighscoreSceneManager : MonoBehaviour
{
    public SceneTransitionManager sceneTransitionManager;

    public TextMeshProUGUI gravityMadnessHighscoreText;
    public TextMeshProUGUI electroLabHighscoreText;

    private void Awake()
    {
        Input.backButtonLeavesApp = false;
    }

    private void Start()
    {
        gravityMadnessHighscoreText.text = PlayerPrefs.GetInt("highscore_GravityMadness", 0).ToString();
        electroLabHighscoreText.text = PlayerPrefs.GetInt("highscore_ElectroLab", 0).ToString();
    }

    public void GoToMainMenu()
    {
        sceneTransitionManager.LoadScene("MainMenuScene");
    }

}
