﻿using UnityEngine;

public class Blink : MonoBehaviour
{
    private CanvasRenderer canvasRenderer;

    public float onTime = 0f;
    public float offTime = 0f;

    private float remainingOnTime;
    private float remainingOffTime;

    private bool isOn = true;

    void Awake()
    {
        canvasRenderer = GetComponentInParent<CanvasRenderer>();
        remainingOnTime = onTime;
        remainingOffTime = offTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (isOn)
        {
            remainingOnTime -= Time.deltaTime;
            if(remainingOnTime < 0f)
            {
                Hide();
            }
        } else
        {
            remainingOffTime -= Time.deltaTime;
            if (remainingOffTime < 0f)
            {
                Show();
            }
        }
    }

    private void Show()
    {
        canvasRenderer.SetAlpha(1f);
        isOn = true;
        ResetTimers();
    }

    private void Hide()
    {
        canvasRenderer.SetAlpha(0f);
        isOn = false;
        ResetTimers();
    }


    private void ResetTimers()
    {
        remainingOnTime = onTime;
        remainingOffTime = offTime;
    }
}
