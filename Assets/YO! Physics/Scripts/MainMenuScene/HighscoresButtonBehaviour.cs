﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighscoresButtonBehaviour : MonoBehaviour
{

    public void OpenHighscoresScene()
    {
        SceneManager.LoadScene("HighscoresScene");
    }

}
