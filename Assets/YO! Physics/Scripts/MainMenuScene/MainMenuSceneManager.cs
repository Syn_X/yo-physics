﻿using DanielLochner.Assets.SimpleScrollSnap;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class MainMenuSceneManager : MonoBehaviour
{
    public AudioMixer audioMixer;

    public SceneTransitionManager sceneTransitionManager;

    public string[] gameScenes;

    private SimpleScrollSnap scrollSnap;

    private int selectedGame = 0;
    private float lastPosition = 0f;
    private int numberOfGames;

    private void Awake()
    {
        Input.backButtonLeavesApp = true;
    }

    void Start()
    {
        GameState.Instance.State = GameStates.MainMenu;

        float musicVolume = PlayerPrefs.GetFloat("volumeMusic", 1);
        float sfxVolume = PlayerPrefs.GetFloat("volumeSFX", 1);

        audioMixer.SetFloat("volumeMusic", Mathf.Log(musicVolume) * 20);
        audioMixer.SetFloat("volumeSFX", Mathf.Log(sfxVolume) * 20);
        audioMixer.SetFloat("volumeUI", Mathf.Log(sfxVolume) * 20);

        scrollSnap = FindObjectOfType<SimpleScrollSnap>();
        numberOfGames = gameScenes.Length;
    }

    public void GamePanelChanged()
    {
        float currentPosition = scrollSnap.Content.position.x;

        if (currentPosition > lastPosition)
        {
            selectedGame--;
            if(selectedGame < 0)
            {
                selectedGame = numberOfGames - 1;
            }
            selectedGame = selectedGame % numberOfGames;
        } else
        {
            selectedGame++;
            selectedGame = selectedGame % numberOfGames;
        }

        lastPosition = scrollSnap.Content.position.x;
    }

    public void PlayGame()
    {
        sceneTransitionManager.LoadScene(gameScenes[selectedGame]);
    }

    public void OpenSettingsMenuScene()
    {
        sceneTransitionManager.LoadScene("SettingsMenuScene");
    }

    public void OpenHighscoresScene()
    {
        sceneTransitionManager.LoadScene("HighscoresScene");
    }

}
