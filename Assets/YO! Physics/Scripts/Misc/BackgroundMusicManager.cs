﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class BackgroundMusicManager : MonoBehaviour
{
    public AudioMixer audioMixer;
    public AudioClip backgroundMusic;

    private void Awake()
    {
        float musicVolume = PlayerPrefs.GetFloat("volumeMusic", 1);
        float sfxVolume = PlayerPrefs.GetFloat("volumeSFX", 1);

        audioMixer.SetFloat("volumeMusic", Mathf.Log(musicVolume) * 20);
        audioMixer.SetFloat("volumeSFX", Mathf.Log(sfxVolume) * 20);
        audioMixer.SetFloat("volumeUI", Mathf.Log(sfxVolume) * 20);

        GameObject[] objs = GameObject.FindGameObjectsWithTag("BackgroundMusic");
        for (int i = 0; i < objs.Length; i++)
        {
            AudioSource source = objs[i].GetComponent<AudioSource>();
            if (source.clip.name != backgroundMusic.name)
            {
                source.clip = backgroundMusic;
                source.Play();
            }
        }
    }

}
