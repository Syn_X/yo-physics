﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DialogResult
{
    None,
    Cancel,
    Yes
}

public class ConfirmationDialog : MonoBehaviour
{

    public DialogResult result = DialogResult.None;

    private void OnEnable()
    {
        result = DialogResult.None;
    }

    public void Yes()
    {
        result = DialogResult.Yes;
    }

    public void No()
    {
        result = DialogResult.Cancel;
    }

}
