﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameState : MonoBehaviour
{
    private static GameState _instance;

    public static GameState Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            _gameState = GameStates.Paused;
        }
    }

    private GameStates _gameState;

    public GameStates State { 
        get { return _gameState; } 
        set { _gameState = value; } 
    }
}

public enum GameStates
{
    Paused,
    Running,
    Ended,
    Info,
    MainMenu
}
