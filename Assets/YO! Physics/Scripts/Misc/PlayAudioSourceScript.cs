﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayAudioSourceScript : MonoBehaviour
{
    public AudioSource audioSource;

    public void PlayOneShot()
    {
        if(!audioSource.isPlaying)
            audioSource.Play();
    }

    public void PlayConcurrent()
    {
        audioSource.Play();
    }

    public void Stop()
    {
        audioSource.Stop();
    }
}
