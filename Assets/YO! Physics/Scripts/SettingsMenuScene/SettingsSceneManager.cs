﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsSceneManager : MonoBehaviour
{
    public SceneTransitionManager sceneTransitionManager;

    public AudioMixer audioMixer;

    public Slider musicSlider;
    public Slider sfxSlider;

    public ConfirmationDialog confirmationDialog;

    private void Awake()
    {
        Input.backButtonLeavesApp = false;
    }

    private void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat("volumeMusic", 1);
        sfxSlider.value = PlayerPrefs.GetFloat("volumeSFX", 1);
    }

    public void GoToMainMenu()
    {
        sceneTransitionManager.LoadScene("MainMenuScene");
    }

    public void ChangeMusicVolume(float value)
    {
        PlayerPrefs.SetFloat("volumeMusic", value);
        audioMixer.SetFloat("volumeMusic", Mathf.Log(value) * 20);
    }

    public void ChangeSFXVolume(float value)
    {
        PlayerPrefs.SetFloat("volumeSFX", value);

        audioMixer.SetFloat("volumeSFX", Mathf.Log(value) * 20);
        audioMixer.SetFloat("volumeUI", Mathf.Log(value) * 20);
    }

    public void ResetHighscorePressed()
    {
        StartCoroutine(ShowConfirmationDialog());
    }

    private void ResetHighscores()
    {
        PlayerPrefs.SetInt("highscore_ElectroLab", 0);
        PlayerPrefs.SetInt("highscore_GravityMadness", 0);
    }

    private IEnumerator ShowConfirmationDialog()
    {
        confirmationDialog.gameObject.SetActive(true);
        CanvasGroup canvas = confirmationDialog.gameObject.GetComponent<CanvasGroup>();
        StartCoroutine(FadeIn(canvas, 0.5f));

        while (confirmationDialog.result == DialogResult.None)
            yield return null;

        if (confirmationDialog.result == DialogResult.Yes)
        {
            ResetHighscores();
            StartCoroutine(FadeOut(canvas, 0.5f));
        }
        else if (confirmationDialog.result == DialogResult.Cancel)
        {
            StartCoroutine(FadeOut(canvas, 0.5f));
        }
    }

    private IEnumerator FadeIn(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha < 1)
        {
            canvasGroup.alpha += Time.deltaTime / fadingTime;
            yield return null;
        }
    }

    private IEnumerator FadeOut(CanvasGroup canvasGroup, float fadingTime)
    {
        while (canvasGroup.alpha > 0)
        {
            canvasGroup.alpha -= Time.deltaTime / fadingTime;
            yield return null;
        }

        canvasGroup.alpha = 0f;
        canvasGroup.gameObject.SetActive(false);
    }
}
